export const MenuData = [
    {
      name: "General",
      items: [
        {
          icon: "apps",
          label: "Home",
          path: "/dashboard/home",
          visible: true,
          enable: true
        },
        {
          icon: "contacts",
          label: "Usuarios",
          path: "/dashboard/users",
          visible: true,
          enable: true
        },
        {
          icon: "people",
          label: "Clientes",
          path: "/dashboard/clients",
          visible: true,
          enable: true
        },
        {
          icon: "card",
          label: "Ventas",
          path: "/dashboard/sells",
          visible: true,
          enable: true
        },
        {
          icon: "analytics",
          label: "Productos",
          path: "/dashboard/products",
          visible: true,
          enable: true
        },
        {
           icon:"car",
           label:"Empleados",
           path:"/dashboard/employee",
           visible:true,
           enable:true


        }
        
        
        
        /*,
        {
          icon: "car",
          label: "Autos",
          path: "/dashboard/cars",
          visible: true,
          enable: true
        },
        {
          icon: "walk",
          label: "Choferes",
          path: "/dashboard/drivers",
          visible: true,
          enable: true
        },
        {
          icon: "analytics",
          label: "Viajes",
          path: "/dashboard/travels",
          visible: true,
          enable: true
        },
        {
          icon: "map",
          label: "Mapa",
          path: "/dashboard/map",
          visible: true,
          enable: true
        },
        {
          icon: "people",
          label: "Grupos",
          path: "/dashboard/groups/list",
          visible: true,
          enable: true
        },
        {
          icon: "information-circle",
          label: "Acerca de",
          path: "/dashboard/about",
          visible: true,
          enable: true
        }*/
      ]
    }
    /* {
      name: "Gestión",
      items: [
        {
          icon: "contacts",
          label: "Clientes",
          path: "/dashboard/clients",
          visible: true,
          enable: true
        },
        {
          icon: "card",
          label: "Pagos",
          path: "/dashboard/payments",
          visible: true,
          enable: true
        }
      ]
    } */
  ];
  