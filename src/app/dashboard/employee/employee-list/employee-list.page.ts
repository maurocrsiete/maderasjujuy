import { Component, OnInit, ViewChild } from '@angular/core';
import { AlertController, ModalController } from '@ionic/angular';
import { EmployeeService } from '../employee.service';
import { ExcelServiceProvider } from '../../../services/excelService/excel-service.service';
import { ScreenService } from '../../../app-screen.service';
import { EmployeeAddComponent } from '../employee-add/employee-add.component';

@Component({
  selector: 'app-employee',
  templateUrl: './employee-list.page.html',
  styleUrls: ['./employee-list.page.scss'],
})
export class EmployeeListPage implements OnInit {
   @ViewChild("inputSearch",{static:true}) inputSearch;
   employees:any;
   employeesSinFiltro:any;
   isLoading=false;
   searchEmployee:Object={employee:"",offset:0, limit:200};
   global_vars:any;
   infiniteRow:number=100;
   hide_sm;
   employee:string="";
   dataStatus:any;
   editModal:any;
   screen:any;

  constructor(
    private service:EmployeeService,
    public modalcontroller:ModalController,
    public alertcontroller:AlertController,
    public exelservice:ExcelServiceProvider,
    private screenService:ScreenService
  ) { 
    screenService.screen.subscribe(_screen =>{
      this.screen=_screen;
    });
    this.getEmployees();
    this.hide_sm=false;
  }

  ngOnInit() {
  }
   getEmployees(){

    this.searchEmployee["employee"] = this.employee;
    this.searchEmployee["offset"] = 0;
    this.searchEmployee["limit"] = this.infiniteRow;
    this.isLoading = true;
    this.service
      .getemployeesGroup(this.searchEmployee)
      .then(data => {
        this.dataStatus = data;
        if (
          typeof this.dataStatus.status !== "undefined" &&
          this.dataStatus.status == false
        ) {
        
        } else {
          this.employees = this.dataStatus.employees;
          console.log(this.employees);
          this.employeesSinFiltro = this.dataStatus;
          
          this.inputSearch.setFocus();
        }
      })
      .catch(err => {
        console.log(err);
      });
    this.isLoading = false;

   }

   async edit(employeeIndex: any) {
    let employee = this.employees[employeeIndex];
    this.editModal = await this.modalcontroller.create({
      component: EmployeeAddComponent,
      cssClass: "my-custom-modal-css",
      componentProps: {
        employeePar: employee,
        idx: employeeIndex,
        onSave: this.saveEmployeeEdited,
        onCancel: this.cancelEmployeeEdition
      }
    });
    return await this.editModal.present();
  }
  async addEmployee() {
    let employeePar = {
      id: null,
      dni: "",
      last_name: "",
      first_name: "",
      apynom:"last_name + first_name",
      phone: "",
      salary: "",
      Fecha: "",
      active: 1,
      "employee-group": null


    };
    this.editModal = await this.modalcontroller.create({
      component: EmployeeAddComponent,
      cssClass: "my-custom-modal-css",
      componentProps: {
        employeePar: employeePar,
        idx: null,
        onSave: this.addNewEmployee,
        onCancel: this.cancelEmployeeEdition
      }
    });
    return await this.editModal.present();
  }
  downloadExcel() {
    this.exelservice.exportAsExcelFile(this.employees, "employees");
  }
  saveEmployeeEdited = (employeeInfo: any, employeeIndex: any) => {
    this.employees[employeeIndex] = employeeInfo;
    this.editModal.dismiss();
  };
  addNewEmployee = (employeeInfo: any, employeeIndex: any) => {
    this.employees.push(employeeInfo);
    this.editModal.dismiss();
  };
  cancelEmployeeEdition = () => {
    this.editModal.dismiss();
  };
  async delete(employeeIndex: any) {
    const alert = await this.alertcontroller.create({
      header: "Quitar Empleado",
      message: "¿Confirma que quiere borrar el Empleado?",
      buttons: [
        {
          text: "Cancelar",
          role: "cancel",
          cssClass: "danger",
          handler: blah => {
            console.log("Confirm Cancel: blah");
          }
        },
        {
          text: "Si",
          handler: () => {
            let employee = this.employees[employeeIndex];
            let par = { id: employee.id, active: 0 };

            this.service
              .deleteEmployee(par)
              .then(data => {
                this.dataStatus = data;
                if (
                  typeof this.dataStatus.status !== "undefined" &&
                  this.dataStatus.status == false
                ) {
                  console.log("Login");
                } else {
                  this.getEmployees();
                }
              })
              .catch(err => {
                console.log(err);
              });
          }
        }
      ]
    });

    await alert.present();
  }
  initializeItems() {
    this.employees = this.employeesSinFiltro;
  }

  getItems(ev: any) {
    console.log(ev);

   
    const val = ev.target.value;

    
    if (val && val.trim() != "") {
      this.employees = this.employees.filter(item => {
        return (
          JSON.stringify(item)
            .toLowerCase()
            .indexOf(val.toLowerCase()) > -1
        );
      });

      if (this.employees.length == 0) {
        this.getEmployees();
      }
    }
  }

}
