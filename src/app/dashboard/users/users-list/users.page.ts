import { Component, OnInit, ViewChild } from "@angular/core";
import { ModalController, AlertController } from "@ionic/angular";
import { UsersService } from "../users.service";
import { UsersAddComponent } from "../users-add/users-add.component";
import { ExcelServiceProvider } from "../../../services/excelService/excel-service.service";
import { ScreenService } from "../../../app-screen.service";

@Component({
  selector: "app-users",
  templateUrl: "./users.page.html",
  styleUrls: ["./users.page.scss"]
})
export class UsersPage implements OnInit {
  @ViewChild("inputSearch", { static: true }) inputSearch;

  users: any;
  usersSinFiltro: any;
  isLoading = false;
  searchUser: Object = { user: "", offset: 0, limit: 200 };
  global_vars: any;
  infiniteRow: Number = 100;
  hide_sm: any;
  user: string = "";
  dataStatus: any;
  editModal: any;
  screen: any;

  constructor(
    private service: UsersService,
    public modalController: ModalController,
    public alertController: AlertController,
    public excelService: ExcelServiceProvider,
    private screenService: ScreenService
  ) {
    screenService.screen.subscribe(_screen => {
      this.screen = _screen;
    });

    this.getUsers();
    /*this.users = [
          {
              id: "2",
              bussinesId: "2",
              first_name: "Mariana",
              last_name: "Dami",
              dni: "0",
              phone: "0",
              email: "mar.dami.18@gmail.com",
              password: "$2y$10$4Fd7RSBF.0MyFRcpCGQczOodg\/\/amUty00qoDVQLzpTJDa1vkYKnG",
              created: "2020-01-29 23:29:44",
              modified: "2020-09-16 22:00:26",
              groupid: "1"
          },
          {
            id: "3",
            bussinesId: "23",
            first_name: "Mariana",
            last_name: "Dami",
            dni: "0",
            phone: "0",
            email: "mar.dami.18@gmail.com",
            password: "$2y$10$4Fd7RSBF.0MyFRcpCGQczOodg\/\/amUty00qoDVQLzpTJDa1vkYKnG",
            created: "2020-01-29 23:29:44",
            modified: "2020-09-16 22:00:26",
            groupid: "1"
        }
      ]
  ;*/
    this.hide_sm = false;
  }

  ngOnInit() {
    //this.getUsers();
  }

  getUsers() {
    this.searchUser["user"] = this.user;
    this.searchUser["offset"] = 0;
    this.searchUser["limit"] = this.infiniteRow;
    this.isLoading = true;
    this.service
      .getUsersGroup(this.searchUser)
      .then(data => {
        this.dataStatus = data;
        if (
          typeof this.dataStatus.status !== "undefined" &&
          this.dataStatus.status == false
        ) {
          //this.navCtrl.setRoot(LoginPage, {}, { animate: true, direction: 'forward' });
        } else {
          this.users = this.dataStatus.users;
          console.log(this.users);
          this.usersSinFiltro = this.dataStatus;
          //console.log(this.users);
          this.inputSearch.setFocus();
        }
      })
      .catch(err => {
        console.log(err);
      });
    this.isLoading = false;
  }

  async edit(userIndex: any) {
    let user = this.users[userIndex];
    this.editModal = await this.modalController.create({
      component: UsersAddComponent,
      cssClass: "my-custom-modal-css",
      componentProps: {
        userPar: user,
        idx: userIndex,
        onSave: this.saveUserEdited,
        onCancel: this.cancelUserEdition
      }
    });
    return await this.editModal.present();
  }

  async addUser() {
    let userPar = {
      id: null,
      dni: "",
      last_name: "",
      first_name: "",
      phone: "",
      email: "",
      password: "",
      active: 1,
      is_admin: 1,
      sys_group_id: 1,
      "user-group": null
    };
    this.editModal = await this.modalController.create({
      component: UsersAddComponent,
      cssClass: "my-custom-modal-css",
      componentProps: {
        userPar: userPar,
        idx: null,
        onSave: this.addNewUser,
        onCancel: this.cancelUserEdition
      }
    });
    return await this.editModal.present();
  }

  downloadExcel() {
    this.excelService.exportAsExcelFile(this.users, "users");
  }

  saveUserEdited = (userInfo: any, userIndex: any) => {
    this.users[userIndex] = userInfo;
    this.editModal.dismiss();
  };

  addNewUser = (userInfo: any, userIndex: any) => {
    this.users.push(userInfo);
    this.editModal.dismiss();
  };

  cancelUserEdition = () => {
    this.editModal.dismiss();
  };

  async delete(userIndex: any) {
    const alert = await this.alertController.create({
      header: "Quitar Usuario",
      message: "¿Confirma que quiere borrar el usuario?",
      buttons: [
        {
          text: "Cancelar",
          role: "cancel",
          cssClass: "danger",
          handler: blah => {
            console.log("Confirm Cancel: blah");
          }
        },
        {
          text: "Si",
          handler: () => {
            let user = this.users[userIndex];
            let par = { id: user.id, active: 0 };

            this.service
              .deleteUser(par)
              .then(data => {
                this.dataStatus = data;
                if (
                  typeof this.dataStatus.status !== "undefined" &&
                  this.dataStatus.status == false
                ) {
                  console.log("Login");
                } else {
                  this.getUsers();
                }
              })
              .catch(err => {
                console.log(err);
              });
          }
        }
      ]
    });

    await alert.present();
  }

  initializeItems() {
    this.users = this.usersSinFiltro;
  }

  getItems(ev: any) {
    console.log(ev);

    /* if (ev.cancelable) {
      this.getUsers();
    } */

    // Reset items back to all of the items
    //this.initializeItems();

    // set val to the value of the searchbar
    const val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != "") {
      this.users = this.users.filter(item => {
        return (
          JSON.stringify(item)
            .toLowerCase()
            .indexOf(val.toLowerCase()) > -1
        );
      });

      if (this.users.length == 0) {
        this.getUsers();
      }
    }
  }
}
