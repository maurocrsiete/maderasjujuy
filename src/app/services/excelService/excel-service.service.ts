import { Injectable } from '@angular/core';
import * as XLSX from 'xlsx';

/*
  Generated class for the ExcelServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

const EXCEL_EXTENSION = '.xlsx';

@Injectable()
export class ExcelServiceProvider {

  constructor() {
  }

  public exportAsExcelFile(json: any[], excelFileName: string): void {
    let a = new Date();
    let fileName = excelFileName + '_' + a.getFullYear().toString() + (a.getMonth() + 1).toString() + a.getDate().toString() + a.getHours().toString() + a.getMinutes().toString() + EXCEL_EXTENSION;
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    XLSX.writeFile(workbook, fileName);
  }

}
