export const environment = {
  production: true,
  API_PATH: "http://maderasjujuy.ddns.net:8080/rst/api/",
  //API_PATH: "http://localhost:8080/rst/api/",
  //API_PATH: "http://127.0.0.1:8080/rst/api/",
  //API_PATH: "http://192.168.0.103:8080/rst/api/",
  //API_PATH: "http://localhost:8080/",
  version: "0.0.1"
};
