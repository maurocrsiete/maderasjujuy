<?php
// required headers
header("Access-Control-Allow-Origin: http://localhost/rst/");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: *");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// required to encode json web token
include_once 'config/core.php';
include_once 'libs/php-jwt-master/src/BeforeValidException.php';
include_once 'libs/php-jwt-master/src/ExpiredException.php';
include_once 'libs/php-jwt-master/src/SignatureInvalidException.php';
include_once 'libs/php-jwt-master/src/JWT.php';
use \Firebase\JWT\JWT;
// files needed to connect to database
include_once 'config/database.php';
include_once 'objects/invoice.php';
//include_once 'objects/invoicedetail.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// instantiate product object
$invoice = new Invoice($db);
//$invoiceDetail = new InvoiceDetail($db);
 
// get posted data
$data = json_decode(file_get_contents("php://input"));
 
// set product property values
$invoice->subtotal = $data->subtotal;
$invoice->descuento = $data->descuento;
$invoice->total = $data->total;
$invoice->clienteid = $data->clienteid;
$invoice->tipocomprobanteid = $data->tipocomprobanteid;
 
$detail = $data->detalle;

$jwt = getallheaders();

// get jwt
//$jwt=isset($data->jwt) ? $data->jwt : "";
$jwt2 = "";
foreach (getallheaders() as $nombre => $valor) {
	if(substr($nombre,0,13) == 'authorization')
	{
		$jwt2 = substr($valor,7,99990);
		//$rest = substr("abcdef", 4, -4);
	}
}
if($jwt2){
    // create the invoice
    if(
        !empty($invoice->subtotal) &&
        !empty($invoice->total) &&
        !empty($invoice->tipocomprobanteid) &&
        $invoice->createInvoice($detail)
    ){
        $token = array(
            "invoiceId" => 1,
            "respuesta" => "safa",
            "message" => "invoice was created."
        );
    
        // set response code
        http_response_code(200);
    
        // display message: invoice was created
        echo json_encode(array($token));
    }
    
    // message if unable to create invoice
    else{
    
        // set response code
        http_response_code(400);
    
        // display message: unable to create invoice
        echo json_encode(array("message" => "Unable to create invoice."));
    }
}
else{
        // set response code
        http_response_code(401);
 
        // tell the product access denied
        echo json_encode(array("message" => $jwt2));
}

?>