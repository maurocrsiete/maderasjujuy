<?php
// required headers
header("Access-Control-Allow-Origin: http://localhost/rst/");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: *");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// required to encode json web token
include_once 'config/core.php';
include_once 'libs/php-jwt-master/src/BeforeValidException.php';
include_once 'libs/php-jwt-master/src/ExpiredException.php';
include_once 'libs/php-jwt-master/src/SignatureInvalidException.php';
include_once 'libs/php-jwt-master/src/JWT.php';
use \Firebase\JWT\JWT;
// files needed to connect to database
include_once 'config/database.php';
include_once 'objects/product.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// instantiate product object
$product = new Product($db);
 
// get posted data
$data = json_decode(file_get_contents("php://input"));
 
// set product property values
$product->CodiBarra = $data->codibarra;
$product->Descripcion = $data->descripcion;
$product->PrecioCosto = $data->preciocosto;
$product->PrecioVenta = $data->precioventa;
$product->Stock = $data->stock;
$product->clienteid = $data->clienteid;
$product->estado = $data->estado;

$jwt = getallheaders();

// get jwt
//$jwt=isset($data->jwt) ? $data->jwt : "";
$jwt2 = "";
foreach (getallheaders() as $nombre => $valor) {
	if(substr($nombre,0,13) == 'authorization')
	{
		$jwt2 = substr($valor,7,99990);
		//$rest = substr("abcdef", 4, -4);
	}
}
if($jwt2){
    if(!$product->productExists())
    {
        if(
            !empty($product->CodiBarra) &&
            !empty($product->Descripcion) &&
            !empty($product->clienteid) &&
            $product->create()
        ){
            $token = array(
                "productId" => 1,
                "respuesta" => "safa",
                "message" => "product was created."
            );
        
            http_response_code(200);
            echo json_encode(array($token));
        }
        else{
            $token = array(
                "productId" => 1,
                "respuesta" => "safa",
                "message" => "Unable to create product."
            );
            http_response_code(400);
            echo json_encode(array($token));
        }
    }
    else
    {
        $token = array(
            "productId" => 1,
            "respuesta" => "safa",
            "message" => "Producto existente"
        );
        http_response_code(400);
        echo json_encode(array($token));
    }
}
else{
        // set response code
        http_response_code(401);
 
        // tell the product access denied
        echo json_encode(array("message" => $jwt2));
}

?>