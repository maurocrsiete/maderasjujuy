<?php
// required headers
header("Access-Control-Allow-Origin: http://localhost/rst/");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// required to encode json web token
include_once 'config/core.php';
include_once 'libs/php-jwt-master/src/BeforeValidException.php';
include_once 'libs/php-jwt-master/src/ExpiredException.php';
include_once 'libs/php-jwt-master/src/SignatureInvalidException.php';
include_once 'libs/php-jwt-master/src/JWT.php';
use \Firebase\JWT\JWT;
 
// files needed to connect to database
include_once 'config/database.php';
include_once 'objects/invoice.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// instantiate invoice object
$invoice = new Invoice($db);
 
$data  =file_get_contents("php://input");
// get posted data
$data = json_decode(file_get_contents("php://input"));
$jwt = getallheaders();

// get jwt
//$jwt=isset($data->jwt) ? $data->jwt : "";
$jwt2 = "";
foreach (getallheaders() as $nombre => $valor) {
	if(substr($nombre,0,13) == 'authorization')
	{
		$jwt2 = substr($valor,7,99990);
		//$rest = substr("abcdef", 4, -4);
	}
}
 
// if jwt is not empty
if($jwt2){
 
    // if decode succeed, show invoice details
    try {
 
        // decode jwt
        $decoded = JWT::decode($jwt2, $key, array('HS256'));

		$invoice->clienteid = $data->clienteid;

		if($data->clienteid)
		{
			$arrayInvo = $invoice->getInvoices($data->clienteid);
			// update the invoice record
			if($arrayInvo){
				// we need to re-generate jwt because invoice details might be different
				$token = array(
					"invoices" => $arrayInvo
				);
				
				$jwt = JWT::encode($token, $key);
				
				// set response code
				http_response_code(200);
				
				// response in json format
				echo json_encode(array("invoices" => $arrayInvo));
			}
			
			// message if unable to update invoice
			else{
				// set response code
				http_response_code(401);
			
				// show error message
				echo json_encode(array("invoiceos" => array()));
			}
		}
		else
		{
			// update the invoice record
			if($invoice->getinvoice($data->CodiBarra)){
				// we need to re-generate jwt because invoice details might be different
				$token = array(
					"invoiceoid" => $invoice->id,
					"descripcion" => $invoice->Descripcion,
					"precio" => floatval($invoice->PrecioVenta),
					"cantidad"=> 1
				);
				
				$jwt = JWT::encode($token, $key);
				
				// set response code
				http_response_code(200);
				
				// response in json format
				echo json_encode(array($token));
			}
			
			// message if unable to update invoice
			else{
				// set response code
				http_response_code(401);
			
				// show error message
				echo json_encode(array("message" => "Unable to update invoice."));
			}
		}
    }
 
   // if decode fails, it means jwt is invalid
	catch (Exception $e){
	 
		// set response code
		http_response_code(401);
	 
		// show error message
		echo json_encode(array(
			"message" => "Access denied.",
			"error" => $e->getMessage(),
			"safa"=> $jwt2
		));
	}
}
 
// show error message if jwt is empty
else{
 
    // set response code
    http_response_code(401);
 
    // tell the invoice access denied
    echo json_encode(array("message" => $jwt));
}
?>