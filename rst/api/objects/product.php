<?php
// 'product' object
class Product{
 
    // database connection and table name
    private $conn;
    private $table_name = "productos";
 
    // object properties
    public $id;
    public $CodiBarra;
    public $Descripcion;
    public $PrecioCosto;
    public $PrecioVenta;
    public $Stock;
    public $clienteid;
    public $estado;
 
    // constructor
    public function __construct($db){
        $this->conn = $db;
    }

    public function getProduct($data,$data2) {

        $query = "SELECT *
        FROM " . $this->table_name . "
        WHERE CodiBarra = " . $data . "
        AND clienteid = " . $data2;

        // prepare the query
        $stmt = $this->conn->prepare($query);

        // sanitize
        $this->CodiBarra=htmlspecialchars(strip_tags($this->CodiBarra));

        // bind the values from the form
        $stmt->bindParam(':data2', $this->data2);

        // execute the query
        if($stmt->execute()){

            // get record details / values
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            // assign values to object properties
            $this->id = $row['Id'];
            $this->CodiBarra = $row['CodiBarra'];
            $this->Descripcion = $row['Descripcion'];
            $this->PrecioCosto = $row['PrecioCosto'];
            $this->PrecioVenta = $row['PrecioVenta'];
            $this->Stock = $row['Stock'];
            $this->clienteid = $row['clienteid'];
            $this->estado = $row['estado'];
 
            return true;
        }    
        return false;
    }

    public function getProductDescrip($data,$data2) {

        $query = "SELECT *
        FROM " . $this->table_name . "
        WHERE Descripcion LIKE('%" . $data . "%')
        AND clienteid = " . $data2;

        // prepare the query
        $stmt = $this->conn->prepare($query);

        // sanitize
        $this->CodiBarra=htmlspecialchars(strip_tags($this->CodiBarra));

        // bind the values from the form
        $stmt->bindParam(':data2', $this->data2);

        // execute the query
        if($stmt->execute()){

            // get record details / values
            $row = $stmt->fetchAll(PDO::FETCH_ASSOC);

            // assign values to object properties
            /*$this->id = $row['Id'];
            $this->CodiBarra = $row['CodiBarra'];
            $this->Descripcion = $row['Descripcion'];
            $this->PrecioCosto = $row['PrecioCosto'];
            $this->PrecioVenta = $row['PrecioVenta'];
            $this->Stock = $row['Stock'];
            $this->clienteid = $row['clienteid'];
            $this->estado = $row['estado'];*/
 
            return $row;
        }    
        return false;
    }

    public function getProductAll($data) {

        $query = "SELECT *
        FROM " . $this->table_name . "
        WHERE clienteid = ". $data;

        // bind the values from the form
        //$stmt->bindParam(':codibarra', $this->codibarra);

        // prepare the query
        $stmt = $this->conn->prepare($query);

        // sanitize
        $this->clienteid=htmlspecialchars(strip_tags($this->clienteid));

        // bind the values from the form
        $stmt->bindParam(':data', $this->data);

        // execute the query
        if($stmt->execute()){

            $row = $stmt->fetchAll(PDO::FETCH_ASSOC);

            // assign values to object properties
            /**$this->id = $row['Id'];
            $this->CodiBarra = $row['CodiBarra'];
            $this->Descripcion = $row['Descripcion'];
            $this->PrecioCosto = $row['PrecioCosto'];
            $this->PrecioVenta = $row['PrecioVenta'];
            $this->Stock = $row['Stock'];*/
 
            return $row;
        }    
        return false;
    }

    function create(){
 
        // insert query
        $query = "INSERT INTO " . $this->table_name . "
                SET
                    CodiBarra = :CodiBarra,
                    Descripcion = :Descripcion,
                    PrecioCosto = :PrecioCosto,
                    PrecioVenta = :PrecioVenta,
                    Stock = :Stock,
                    clienteid = :clienteid,
                    estado = :estado";
     
        // prepare the query
        $stmt = $this->conn->prepare($query);
     
        // sanitize
        $this->CodiBarra=htmlspecialchars(strip_tags($this->CodiBarra));
        $this->Descripcion=htmlspecialchars(strip_tags($this->Descripcion));
        $this->PrecioCosto=htmlspecialchars(strip_tags($this->PrecioCosto));
        $this->PrecioVenta=htmlspecialchars(strip_tags($this->PrecioVenta));
        $this->Stock=htmlspecialchars(strip_tags($this->Stock));
        $this->clienteid=htmlspecialchars(strip_tags($this->clienteid));
        $this->estado=htmlspecialchars(strip_tags($this->estado));
     
        // bind the values
        $stmt->bindParam(':CodiBarra', $this->CodiBarra);
        $stmt->bindParam(':Descripcion', $this->Descripcion);
        $stmt->bindParam(':PrecioCosto', $this->PrecioCosto);
        $stmt->bindParam(':PrecioVenta', $this->PrecioVenta);
        $stmt->bindParam(':Stock', $this->Stock);
        $stmt->bindParam(':clienteid', $this->clienteid);
        $stmt->bindParam(':estado', $this->estado);
        
     
        // execute the query, also check if query was successful
        if($stmt->execute()){
            return true;
        }
     
        return false;
    }

    function productExists(){
 
        // query to check if email exists
        $query = "SELECT *
                FROM " . $this->table_name . "
                WHERE CodiBarra = :CodiBarra
                AND clienteid = :clienteid
                LIMIT 0,1";
     
        // prepare the query
        $stmt = $this->conn->prepare( $query );
     
        // sanitize
        $this->CodiBarra=htmlspecialchars(strip_tags($this->CodiBarra));
        $this->clienteid=htmlspecialchars(strip_tags($this->clienteid));
     
        // bind given email value
        $stmt->bindParam(":CodiBarra", $this->CodiBarra);
        $stmt->bindParam(":clienteid", $this->clienteid);
     
        // execute the query
        $stmt->execute();
     
        // get number of rows
        $num = $stmt->rowCount();
     
        // if email exists, assign values to object properties for easy access and use for php sessions
        if($num>0){
     
            // get record details / values
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
     
            // assign values to object properties
            $this->id = $row['Id'];
            $this->CodiBarra = $row['CodiBarra'];
            $this->Descripcion = $row['Descripcion'];
            $this->PrecioCosto = $row['PrecioCosto'];
            $this->PrecioVenta = $row['PrecioVenta'];
            $this->Stock = $row['Stock'];
            $this->clienteid = $row['clienteid'];
            $this->estado = $row['estado'];
     
            // return false because email exists in the database
            return true;
        }
     
        // return true if email does not exist in the database
        return false;
    }

    public function getStock($data,$array) {

        $query = "SELECT *
        FROM " . $this->table_name . "
        WHERE clienteid = :clienteid";

        // bind the values from the form
        //$stmt->bindParam(':codibarra', $this->codibarra);

        // prepare the query
        $stmt = $this->conn->prepare($query);

        // sanitize
        $this->clienteid=htmlspecialchars(strip_tags($this->clienteid));

        // bind the values from the form
        $stmt->bindParam(':clienteid', $this->data);

        // execute the query
        if($stmt->execute()){

            // get record details / values
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            $array = $row;

            // assign values to object properties
            /*$this->id = $row['Id'];
            $this->CodiBarra = $row['CodiBarra'];
            $this->Descripcion = $row['Descripcion'];
            $this->PrecioCosto = $row['PrecioCosto'];
            $this->PrecioVenta = $row['PrecioVenta'];
            $this->Stock = $row['Stock'];*/
 
            return $array;
        }    
        return false;
    }
    public function update(){
     
        // if no posted password, do not update the password
        $query = "UPDATE " . $this->table_name . "
                SET
                    CodiBarra = :CodiBarra,
                    Descripcion = :Descripcion,
                    PrecioCosto = :PrecioCosto,
                    PrecioVenta = :PrecioVenta,
                    Stock = :Stock
                WHERE id = :id";
     
        // prepare the query
        $stmt = $this->conn->prepare($query);
     
        // sanitize
        $this->CodiBarra=htmlspecialchars(strip_tags($this->CodiBarra));
        $this->Descripcion=htmlspecialchars(strip_tags($this->Descripcion));
        $this->PrecioCosto=htmlspecialchars(strip_tags($this->PrecioCosto));
        $this->PrecioVenta=htmlspecialchars(strip_tags($this->PrecioVenta));
        $this->Stock=htmlspecialchars(strip_tags($this->Stock));
     
        // bind the values from the form
        $stmt->bindParam(':CodiBarra', $this->CodiBarra);
        $stmt->bindParam(':Descripcion', $this->Descripcion);
        $stmt->bindParam(':PrecioCosto', $this->PrecioCosto);
        $stmt->bindParam(':PrecioVenta', $this->PrecioVenta);
        $stmt->bindParam(':Stock', $this->Stock);
     
        // unique ID of record to be edited
        $stmt->bindParam(':id', $this->id);
     
        // execute the query
        if($stmt->execute()){
            return true;
        }
     
        return false;
    }

    public function updatestate(){
     
        // if no posted password, do not update the password
        $query = "UPDATE " . $this->table_name . "
                SET
                    estado = :estado
                WHERE id = :id";
     
        // prepare the query
        $stmt = $this->conn->prepare($query);
     
        // sanitize
        $this->estado=htmlspecialchars(strip_tags($this->estado));
     
        // bind the values from the form
        $stmt->bindParam(':estado', $this->estado);
     
        // unique ID of record to be edited
        $stmt->bindParam(':id', $this->id);
     
        // execute the query
        if($stmt->execute()){
            return true;
        }
     
        return false;
    }

}